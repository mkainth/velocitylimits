package mockdb

import (
	"bitbucket.org/mkainth/velocitylimits/domain/model"
	"errors"
	"fmt"
	"sync"
)

// LoadFundsEventResponse is an internal data structure used for
// tracking accepted and declined LoadFundsEvents.
type LoadFundsEventResponse struct {
	Event    *model.LoadFundsEvent
	Response *model.LoadFundsResponse
}

// CustomerData is an internal data structure used by the CustomerMockDB database implementation
type CustomerData struct {
	MetaData                  *model.Customer
	LoadFundsCollection       model.LoadFundsCollection
	LoadFundsEventLookUpTable map[string]*LoadFundsEventResponse
	Mutex                     *sync.Mutex
}

// CustomerMockDB is an implementation of the CustomerDataInterface.
// This implementation is only for test purposes and not suitable for production use.
type CustomerMockDB struct {
	Customers map[string]*CustomerData
}

// New will return a new instance of CustomerMockDB
func New() *CustomerMockDB {
	return &CustomerMockDB{
		Customers: map[string]*CustomerData{},
	}
}

// Get retrieves a customer from the database.
func (db *CustomerMockDB) Get(customerID string) (*model.Customer, error) {
	if customer, ok := db.Customers[customerID]; !ok {
		return nil, errors.New("unable to find customer")
	} else {
		return customer.MetaData, nil
	}
}

// Insert adds a user to a database.
func (db *CustomerMockDB) Insert(customer *model.Customer) (*model.Customer, error) {
	if _, ok := db.Customers[customer.ID]; !ok {
		db.Customers[customer.ID] = &CustomerData{
			MetaData:                  customer,
			LoadFundsCollection:       model.LoadFundsCollection{},
			LoadFundsEventLookUpTable: map[string]*LoadFundsEventResponse{},
			Mutex:                     &sync.Mutex{},
		}
		return customer, nil
	} else {
		return nil, errors.New("customer already exists")
	}
}

// getCustomerData is a helper method used to obtain customer data.
func (db *CustomerMockDB) getCustomerData(customerID string) (*CustomerData, error) {
	var customerData *CustomerData
	var ok bool
	if customerData, ok = db.Customers[customerID]; !ok {
		return nil, errors.New("unable to find customer")
	}
	return customerData, nil
}

// GetLoadFundsEvent retrieves a LoadFundsEvent for a specified customerID and eventID.
func (db *CustomerMockDB) GetLoadFundsEvent(customerID, eventID string) (*model.LoadFundsEvent, error) {
	customerData, err := db.getCustomerData(customerID)
	if err != nil {
		return nil, err
	}
	var event *LoadFundsEventResponse
	var ok bool
	if event, ok = customerData.LoadFundsEventLookUpTable[eventID]; !ok {
		return nil, fmt.Errorf("event with ID '%s' not found", eventID)
	}
	return event.Event, nil
}

// ListLoadFundsEvent will list the latest n LoadFundsEvents in ascending order.
// NOTE: a copy of each Event structure will be made to avoid any
func (db *CustomerMockDB) ListLoadFundsEvent(customerID string, n int) (*model.LoadFundsCollection, error) {
	customerData, err := db.getCustomerData(customerID)
	if err != nil {
		return nil, err
	}
	lenEvents := len(customerData.LoadFundsCollection.Events)
	if n > lenEvents {
		return &model.LoadFundsCollection{
			Events: customerData.LoadFundsCollection.Events[:],
			Length: lenEvents,
		}, nil
	}
	return &model.LoadFundsCollection{
		Events: customerData.LoadFundsCollection.Events[customerData.LoadFundsCollection.Length-n:],
		Length: n,
	}, nil
}

// AddFunds will update the customer account balance and insert a LoadFundsEvent
// into the customer database. This operation is atomic.
func (db *CustomerMockDB) AddFunds(event *model.LoadFundsEvent, accepted bool) (*model.LoadFundsResponse, error) {
	customerData, err := db.getCustomerData(event.CustomerID)
	if err != nil {
		return nil, err
	}
	customerData.Mutex.Lock()
	defer customerData.Mutex.Unlock()
	// [START update LoadFundsLookUpTable]
	response := &model.LoadFundsResponse{
		ID:         event.ID,
		CustomerID: event.CustomerID,
		Accepted:   accepted,
	}
	customerData.LoadFundsEventLookUpTable[event.ID] = &LoadFundsEventResponse{
		Event:    event,
		Response: response,
	}
	// if the LoadFundsEvent was declined, we do not need to update balance or LoadFundsCollection
	if accepted == false {
		return response, nil
	}
	// [END update LoadFundsLookUpTable]
	// [START update the customer balance]
	loadAmount, err := convertCurrencyStringToFloat64(event.LoadAmount)
	if err != nil {
		return nil, err
	}
	customerData.MetaData.Balance += loadAmount
	// [END update the customer balance]
	// [START add new LoadFundsEvent to collection]
	customerData.LoadFundsCollection.Events = append(customerData.LoadFundsCollection.Events, *event)
	customerData.LoadFundsCollection.Length += 1
	// [END add new LoadFundsEvent to collection]
	return response, nil
}
