package mockdb

import (
	"strconv"
	"strings"
)

// convertCurrencyStringToFloat64 will convert a currency string to a floating point number.
// For example: "$5123.09" => 5123.09
func convertCurrencyStringToFloat64(currencyString string) (float64, error) {
	currencyString = strings.TrimPrefix(currencyString, "$")
	f, err := strconv.ParseFloat(currencyString, 64)
	if err != nil {
		return 0, err
	}
	return f, nil
}
