// This package contains interfaces for accessing persistent data.
package database

import (
	"bitbucket.org/mkainth/velocitylimits/domain/model"
)

// CustomerDataInterface is a database interface for managing customer data
type CustomerDataInterface interface {
	// Get retrieves a customer from the database.
	Get(customerID string) (*model.Customer, error)
	// Insert adds a user to a database.
	Insert(customer *model.Customer) (*model.Customer, error)
	// GetFundEvent retrieves a LoadFundsEvent for a specified customerID and eventID.
	GetLoadFundsEvent(customerID, eventID string) (*model.LoadFundsEvent, error)
	// ListFundsEvent will list the latest n LoadFundsEvents in ascending order.
	ListLoadFundsEvent(customerID string, n int) (*model.LoadFundsCollection, error)
	// AddFunds will update the customer account balance if the funds were accepted.
	// All LoadFundsEvents (accepted and declined) will be indexed.
	// NOTE: This operation is atomic.
	AddFunds(event *model.LoadFundsEvent, accepted bool) (*model.LoadFundsResponse, error)
}
