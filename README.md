# Velocity Limits - Solution

Author: Michael Kainth

[Original Description](REQUIREMENTS.md)

## Testing

To run all tests, please execute the following command in the root folder of this repository.

```shell
make test
```
