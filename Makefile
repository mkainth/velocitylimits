EXPECTED_OUTPUT_FILE=cmd/output.txt
GENERATED_OUTPUT_FILE=cmd/generated.txt
INPUT_FILE=cmd/input.txt
test:
	go test ./...
	go run cmd/*.go test -input $(INPUT_FILE) > $(GENERATED_OUTPUT_FILE)
	DIFF=$(diff $(EXPECTED_OUTPUT_FILE) $(GENERATED_OUTPUT_FILE))
	if [ "$(DIFF)" != "" ]; then \
  		echo Test Failed: Generated output '$(GENERATED_OUTPUT_FILE)' did not match expected output '$(EXPECTED_OUTPUT_FILE)'; \
	else \
  		echo Tests were successful!; \
	fi
