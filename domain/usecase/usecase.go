// This package contains all business logic associated with the Customer domain.
package usecase

import (
	"context"
	"bitbucket.org/mkainth/velocitylimits/domain/model"
)

// CustomerLifeCycleUseCaseInterface is used to manage the customer account lifecycle.
type CustomerLifeCycleUseCaseInterface interface {
	// CreateAccount creates a new customer account, essentially inserting a new record into a database.
	CreateAccount(ctx context.Context, customer *model.Customer) (*model.Customer, error)
	// RetrieveAccount gets a customer account given a customerID string.
	RetrieveAccount(ctx context.Context, customerID string) (*model.Customer, error)
	// LoadFunds will attempt to load funds into a customer account and return an event response.
	LoadFunds(ctx context.Context, input *model.LoadFundsEvent) (*model.LoadFundsResponse, error)
}