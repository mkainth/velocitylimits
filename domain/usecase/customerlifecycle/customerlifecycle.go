package customerlifecycle

import (
	"bitbucket.org/mkainth/velocitylimits/database"
	"bitbucket.org/mkainth/velocitylimits/domain/model"
	"context"
)

// CustomerLifeCycleUseCase implements CustomerLifeCycleUseCaseInterface
type CustomerLifeCycleUseCase struct {
	DataService database.CustomerDataInterface
}

// LoadFunds will attempt to load funds into a customer account and return an event response.
// Funds can only be loaded if the following velocity conditions are met:
// 		1. A maximum of $5,000 can be loaded per day.
//		2. A maximum of $20,000 can be loaded per week. (The start of a week is on Monday)
//		3. A maximum of 3 loads can be made per day.
// If the event id is already in use by the customer, a nil response is returned.
func (useCase *CustomerLifeCycleUseCase) LoadFunds(ctx context.Context, input *model.LoadFundsEvent) (*model.LoadFundsResponse, error) {
	// check if LoadFundsEvent already exists for customer
	event, err := useCase.DataService.GetLoadFundsEvent(input.CustomerID, input.ID)
	if event != nil {
		return nil, nil
	}
	// get the latest 21 LoadFundsEvents
	collection, err := useCase.DataService.ListLoadFundsEvent(input.CustomerID, 21)
	if err != nil {
		return nil, err
	}
	// [START check velocity conditions]
	newEventAmount, err := convertCurrencyStringToFloat64(input.LoadAmount)
	if err != nil {
		return nil, err
	}
	var sumDailyAmount float64 = newEventAmount
	var sumWeeklyAmount float64 = newEventAmount
	var countDailyLoads int = 1
	for i := collection.Length - 1; i >= 0; i-- {
		weekComparison, err := compareWeek(collection.Events[i].Time, input.Time)
		if err != nil {
			return nil, err
		}
		if weekComparison == 0 {
			amount, err := convertCurrencyStringToFloat64(collection.Events[i].LoadAmount)
			if err != nil {
				return nil, err
			}
			sumWeeklyAmount += amount
			dayComparison, err := compareDay(collection.Events[i].Time, input.Time)
			if dayComparison == 0 {
				sumDailyAmount += amount
				countDailyLoads += 1
			}
		}
		if weekComparison == -1 {
			// since collection is in ascending order,
			// we reached a point where we are passed the current week's timeframe
			break
		}
	}
	var accepted bool = false
	if sumDailyAmount <= 5000 && countDailyLoads <= 3 && sumWeeklyAmount <= 20000 {
		accepted = true
	}
	return useCase.DataService.AddFunds(input, accepted)
	// [END check velocity conditions]
}

// CreateAccount creates a new customer account, essentially inserting a new record into a database.
func (useCase *CustomerLifeCycleUseCase) CreateAccount(ctx context.Context, customer *model.Customer) (*model.Customer, error) {
	return useCase.DataService.Insert(customer)
}

// RetrieveAccount gets a customer account given a customerID string.
func (useCase *CustomerLifeCycleUseCase)  RetrieveAccount(ctx context.Context, customerID string) (*model.Customer, error) {
	return useCase.DataService.Get(customerID)
}