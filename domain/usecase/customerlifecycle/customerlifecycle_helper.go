package customerlifecycle

import (
	"time"
	"strings"
	"strconv"
)

// getDateTime will convert a timestamp string with format '2006-01-02T15:04:05Z' to a 'time.Time' type.
func getDateTime(date string) (time.Time, error) {
	return time.Parse("2006-01-02T15:04:05Z", date)
}

// compareSameDay will return the following results:
// 		a. returns 1 if date2 is on a day after date1.
// 		b. returns 0 if date2 and date1 are on the same day.
// 		c. returns -1 if date2 is on a day before date1.
// 		d. returns -2 if an error occurred.
func compareDay(date1, date2 string) (int, error) {
	date1Time, err := getDateTime(date1)
	if err != nil {
		return -2, err
	}
	date2Time, err := getDateTime(date2)
	if err != nil {
		return -2, err
	}
	y1,m1,d1 := date1Time.Date()
	date1BeginningOfDay := time.Date(y1,m1,d1,0,0,0,0,time.UTC)
	day, _ := time.ParseDuration("23h59m59s")
	date1EndOfDay := date1BeginningOfDay.Add(day)
	if date2Time.After(date1EndOfDay) {
		return 1, nil
	}
	if date2Time.Before(date1BeginningOfDay) {
		return -1, nil
	}
	return 0, nil
}

// compareWeek will return the following results:
// 		a. returns 1 if date2 is on a week after date1.
// 		b. returns 0 if date2 and date1 are on the same week.
// 		c. returns -1 if date2 is on a week before date1.
// 		d. returns -2 if an error occurred.
func compareWeek(date1, date2 string) (int, error) {
	date1Time, err := getDateTime(date1)
	if err != nil {
		return -2, err
	}
	date2Time, err := getDateTime(date2)
	if err != nil {
		return -2, err
	}
	y1,m1,d1 := date1Time.Date()
	weekday := (date1Time.Weekday() + 6) % 7
	beginningOfWeek := time.Date(y1,m1,d1-int(weekday),0,0,0,0,time.UTC)

	week, _ := time.ParseDuration("167h59m59s")
	endOfWeek := beginningOfWeek.Add(week)
	if date2Time.After(endOfWeek) {
		return 1, nil
	}
	if date2Time.Before(beginningOfWeek) {
		return -1, nil
	}
	return 0, nil
}

// convertCurrencyStringToFloat64 will convert a currency string to a floating point number.
// For example: "$5123.09" => 5123.09
func convertCurrencyStringToFloat64(currencyString string) (float64, error) {
	currencyString = strings.TrimPrefix(currencyString, "$")
	f, err := strconv.ParseFloat(currencyString, 64)
	if err != nil {
		return 0, err
	}
	return f, nil
}