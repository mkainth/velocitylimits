package customerlifecycle

import "testing"

type TimeTestCase struct {
	Date1 string
	Date2 string
	Expected int
}

var CompareDayTests = []*TimeTestCase{
	{
		Date1: "2000-01-01T00:00:00Z",
		Date2: "2000-01-01T23:59:59Z",
		Expected: 0,
	},
	{
		Date1: "2000-01-01T23:59:59Z",
		Date2: "2000-01-02T00:00:00Z",
		Expected: 1,
	},
	{
		Date1: "2000-01-02T00:00:00Z",
		Date2: "2000-01-01T23:59:59Z",
		Expected: -1,
	},
}

var CompareWeekTests = []*TimeTestCase{
	{
		Date1: "2020-08-23T23:59:59Z",
		Date2: "2020-08-24T00:00:00Z",
		Expected: 1,
	},
	{
		Date1: "2020-08-24T00:00:00Z",
		Date2: "2020-08-23T23:59:59Z",
		Expected: -1,
	},
	{
		Date1: "2020-08-24T00:00:00Z",
		Date2: "2020-08-30T23:59:59Z",
		Expected: 0,
	},
}

func TestCompareDay(t *testing.T) {
	for i, test := range CompareDayTests {
		result, err := compareDay(test.Date1, test.Date2)
		if err != nil {
			t.Fatal(err)
		}
		if result != test.Expected {
			t.Fatalf("TestCompareDay #%v failed: %v, expected: %v, got: %v\n", i, *test, test.Expected, result)
		}
	}
}

func TestCompareWeek(t *testing.T) {
	for i, test := range CompareWeekTests {
		result, err := compareWeek(test.Date1, test.Date2)
		if err != nil {
			t.Fatal(err)
		}
		if result != test.Expected {
			t.Fatalf("TestCompareWeek #%v failed: %v, expected: %v, got: %v\n", i, *test, test.Expected, result)
		}
	}
}
