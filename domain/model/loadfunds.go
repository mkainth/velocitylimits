package model

import (
	"errors"
)

// LoadFundsEvent represents an attempt to load funds into a customer account.
type LoadFundsEvent struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	LoadAmount string `json:"load_amount"`
	Time       string `json:"time"`
}

// Validate ensures that that input data is valid
func (event *LoadFundsEvent) Validate() error {
	if event.ID == "" || event.CustomerID == "" || event.LoadAmount == "" || event.Time == "" {
		return errors.New("")
	}
	return nil
}

// LoadFundsResponse contains the response data of a LoadFundsEvent.
type LoadFundsResponse struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	Accepted   bool   `json:"accepted"`
}

// LoadFundsCollection is a list of LoadFundsEvent in ascending order
type LoadFundsCollection struct {
	Events []LoadFundsEvent
	Length int
}
