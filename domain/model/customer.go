package model

const (
	STANDARD_TIER = "standard"
	PREMIUM_TIER = "premium"
)

// Customer represents a customer account.
// Valid Tiers are {"standard", "premium"}
type Customer struct {
	ID        string `json:"id"`
	Tier      string `json:"tier"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Balance   float64 `json:"balance"`
}


