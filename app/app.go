// This package makes use of dependency injection to create the application service.
// A Factory pattern along with a configuration file can be used later on to launch
// infrastructure/database agnostic deployments.
package app

import (
	"bitbucket.org/mkainth/velocitylimits/domain/usecase"
	"bitbucket.org/mkainth/velocitylimits/domain/usecase/customerlifecycle"
	"bitbucket.org/mkainth/velocitylimits/database/customer/mockdb"
)

// CustomerLifeCycleService encapsulates a use case interface (business logic)
// for the Customer Life Cycle.
type CustomerLifeCycleService struct {
	UseCase usecase.CustomerLifeCycleUseCaseInterface
}

// NewCustomerLifeCycleTestService creates a new CustomerLifeCycleService
func NewCustomerLifeCycleTestService() *CustomerLifeCycleService {
	newCustomerMockDB := mockdb.New()
	useCase := &customerlifecycle.CustomerLifeCycleUseCase{
		DataService: newCustomerMockDB,
	}
	return &CustomerLifeCycleService{
		UseCase: useCase,
	}
}
