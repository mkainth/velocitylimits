package main

import (
	"bitbucket.org/mkainth/velocitylimits/app"
	"bitbucket.org/mkainth/velocitylimits/domain/model"
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

// TestApplication will initialize a new CustomerLifeCycleTestService
// and input test cases given an input filepath stored in commandFlags.
// Output will be logged directly to stdout.
func TestApplication() {
	ctx := context.Background()
	// [START setup input file reader]
	inputFilePath := *commandFlags["input"]
	inputFile, err := os.Open(inputFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer inputFile.Close()
	inputFileReader := bufio.NewReader(inputFile)
	// [END setup input file reader]
	// [START testing application]
	testService := app.NewCustomerLifeCycleTestService()
	inputLine, err := inputFileReader.ReadBytes('\n')
	for ; ; inputLine, err = inputFileReader.ReadBytes('\n') {
		if err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatalf("error: %v", err)
			}
		}
		event := model.LoadFundsEvent{}
		err = json.Unmarshal(inputLine, &event)
		if err != nil {
			log.Fatalf("unable to unmarshal line '%s' in file '%s' due to error: '%v'\n", string(inputLine), inputFilePath, err)
		}

		// [START test business logic]
		// check if customer exists, if not create the customer (for test purposes)
		if _, err = testService.UseCase.RetrieveAccount(ctx, event.CustomerID); err != nil {
			_, err := testService.UseCase.CreateAccount(ctx, &model.Customer{
				ID:        event.CustomerID,
				Tier:      model.STANDARD_TIER,
				FirstName: "Ruby",
				LastName:  "Tuesday",
				Balance:   0,
			})
			if err != nil {
				log.Fatalf("unable to create customer '%s' due to internal error\n", event.CustomerID)
			}
		}
		// attempt to load funds to the customer account
		response, err := testService.UseCase.LoadFunds(ctx, &event)
		if err != nil {
			log.Fatal(err)
		}
		if response != nil {
			responseBytes, err := json.Marshal(response)
			if err != nil {
				log.Fatalf("unable to Marshal response '%v'\n", response)
			}
			fmt.Println(string(responseBytes))
		}
		// [END test business logic
	}
	// [END testing application]
}
