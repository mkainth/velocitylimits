package main

import (
	"flag"
	"os"
)

var command string
var commandFlags map[string]*string

func init() {
	// Verify that a valid command has been provided
	if len(os.Args) < 2 {
		panic("a valid subcommand is required. For example: \"test\"")
	}
	command = os.Args[1]
	if command != "test" {
		panic("a valid subcommand is required. For example: \"test\"")
	}
	testCommand := flag.NewFlagSet("test", flag.ExitOnError)
	commandFlags = map[string]*string{
		"input": testCommand.String("input", "input.txt", "the input test file."),
	}
	testCommand.Parse(os.Args[2:])
}

func main() {
	if command == "test" {
		TestApplication()
	}
}
